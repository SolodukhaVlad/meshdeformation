﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public enum ButtonMoveType
{
    Forward,
    Backward,
    Left,
    Right,
    RotateLeft,
    RotateRight
}
public class ButtonController : MonoBehaviour, IPointerUpHandler, IPointerDownHandler
{
    public  MoveController moveController;
    public  ButtonMoveType moveType;
    private float          pointerDownTime = 0;
    private bool           isPointerDownNow = false;

    public void Update()
    {
        if (isPointerDownNow)
        {
            moveController.Move(pointerDownTime, moveType);
            pointerDownTime += Time.deltaTime;
            
        }
    }
    public void OnPointerDown(PointerEventData eventData)
    {
        isPointerDownNow = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        isPointerDownNow = false;
        pointerDownTime = 0;
    }

}
