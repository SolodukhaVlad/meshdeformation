﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveController : MonoBehaviour
{
    private CharacterController  characterController;

    public float currentSpeed           = 1f;
    public float maxSpeed               = 3f;
    public float currentRotate          = 1f;
    public float pointerDownTimeLimit   = 2f; 
    // Start is called before the first frame update
    private void Awake()
    {
        characterController = GetComponent<CharacterController>();
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void Move(float _pointerDownTime, ButtonMoveType _moveType)
    {
       switch(_moveType)
        {
            case ButtonMoveType.Forward:
                {
                    MoveForward(_pointerDownTime);
                    break;
                }
            case ButtonMoveType.Backward:
                {
                    MoveBack(_pointerDownTime);
                    break;
                }
            case ButtonMoveType.Left:
                {
                    MoveLeft(_pointerDownTime);
                    break;
                }
            case ButtonMoveType.Right:
                {
                    MoveRight(_pointerDownTime);
                    break;
                }
            case ButtonMoveType.RotateRight:
                {
                    RotateRight(_pointerDownTime);
                    break;
                }
            case ButtonMoveType.RotateLeft:
                {
                    RotateLeft(_pointerDownTime);
                    break;
                }
            default:
                {
                    break;
                }
        }
    }
    public void MoveForward(float _pointerDownTime)
    {
        float moveSpeed = (_pointerDownTime >= pointerDownTimeLimit) ? maxSpeed : currentSpeed;
        characterController.Move(transform.forward * moveSpeed * Time.deltaTime);
    }

    public void MoveLeft(float _pointerDownTime)
    {
        float moveSpeed = (_pointerDownTime >= pointerDownTimeLimit) ? maxSpeed : currentSpeed;
        characterController.Move(transform.right* -1 *moveSpeed*Time.deltaTime);
    }

    public void MoveRight(float _pointerDownTime)
    {
        float moveSpeed = (_pointerDownTime >= pointerDownTimeLimit) ? maxSpeed : currentSpeed;
        characterController.Move(transform.right * moveSpeed * Time.deltaTime);
    }

    public void MoveBack(float _pointerDownTime)
    {
        float moveSpeed = (_pointerDownTime >= pointerDownTimeLimit) ? maxSpeed : currentSpeed;
        characterController.Move(transform.forward * -1 * moveSpeed * Time.deltaTime);
    }

    public void RotateRight(float _pointerDownTime)
    {
        float rotateSpeed = (_pointerDownTime >= pointerDownTimeLimit) ? currentRotate*2 : currentRotate;
        characterController.transform.Rotate(new Vector3(0, rotateSpeed * Time.deltaTime));
    }

    public void RotateLeft(float _pointerDownTime)
    {
        float rotateSpeed = (_pointerDownTime >= pointerDownTimeLimit) ? currentRotate * 2 : currentRotate;
        characterController.transform.Rotate(new Vector3(0, rotateSpeed * -1 * Time.deltaTime));
    }
}
