﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vertice
{
    public Vector3 vertice;
    public int index;

    public Vertice(Vector3 _vertice, int _index)
    {
        vertice = _vertice;
        index = _index;
    }
}
[RequireComponent(typeof(MeshFilter))]
public class MeshDeformer : MonoBehaviour
{
    MeshCollider meshCollider;

    void Start()
    {

    }

}
