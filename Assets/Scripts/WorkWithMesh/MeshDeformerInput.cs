﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MeshDeformerInput : MonoBehaviour
{
    public float forceValue = 0.01f;
    public float size = 1;

    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            HandleInput();
        }
        else if(Input.GetMouseButton(1))
        {
            HandleInput(false);
        }
    }

    public void HandleInput(bool leftClick = true)
    {
        int layerMask = 8;
        RaycastHit hit;
        if (!Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit))
            return;

        MeshCollider meshCollider = hit.collider as MeshCollider;
        MeshDeformer meshDeform = hit.collider.GetComponent<MeshDeformer>();
        if (meshCollider == null || meshCollider.sharedMesh == null || meshDeform == null || hit.collider.gameObject.layer!= layerMask)
            return;

        Mesh mesh = meshCollider.sharedMesh;
        Vector3[] vertices = mesh.vertices;
        int[] triangles = mesh.triangles;
        List<Vertice> verticesInRange = new List<Vertice>(); //Список обьектов типа index/vertex

        for (int i=0;i < vertices.Length;i++)
        {
            float difference = Vector3.Distance(meshCollider.transform.TransformPoint(vertices[i]), hit.point);   //ищу разницу между точкой попадания луча и каждого вертекса
            if (difference <= size)//переменная size это и есть тот рендж,в котором мы должны двигать вертексы
            {
                verticesInRange.Add(new Vertice(vertices[i],i));        //если вертекс меньше рейнджа,значит мы добавляем его в массив,елементы которого потом увеличим
            }
        }

        if(verticesInRange.Count==0)
        {
            return;
        }

        for (int i = 0; i < verticesInRange.Count; i++) //пробегаемся по масиву вертексов из рейнджа,увеличиваем их и присваиваем главному массиву
        {
            float currentForce = (leftClick) ? forceValue : forceValue * -1;
            verticesInRange[i].vertice += hit.normal * currentForce;
            vertices[verticesInRange[i].index] = verticesInRange[i].vertice;
        }

        mesh.vertices = vertices;   //присваиваем массив вертексов главному массиву
        mesh.RecalculateNormals();
        mesh.RecalculateBounds();
    }

}
